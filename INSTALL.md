## Manual Installation

### Prerequisite

 - This requires the [dependencies][DEPENDENCIES.md] to be already installed.
 - This needs root / sudo privileges for dependency and package installation.

### System-wide install


```console
// build
$ git clone https://gitlab.gnome.org/crvi/gnome-activity-journal.git
$ cd gnome-activity-journal
$ ./setup.py build

// install from same dir
$ sudo pip3 install .
$ sudo glib-compile-schemas /usr/local/share/glib-2.0/schemas/

// run
$ /usr/local/bin/gnome-activity-journal
```

### Show install information

```console
$ pip3 show gnome-activity-journal
```

### System-wide uninstall

```console
$ sudo pip3 uninstall gnome-activity-journal
$ sudo glib-compile-schemas /usr/local/share/glib-2.0/schemas/
```

## Run Without Installation

### Prerequisite

 - This requires the [dependencies][DEPENDENCIES.md] to be already installed.
 - This needs root / sudo privileges for dependency installation.

Here, the app can be built and run directly from source tree, without
installation.

```console
// install dependencies from your distro
# <install deps as root>

// checkout from git
$ git clone https://gitlab.gnome.org/crvi/gnome-activity-journal.git

// run
$ cd gnome-activity-journal
$ ./run.sh
```

[DEPENDENCIES.md]: DEPENDENCIES.md#dependencies
