# Asturian translation for gnome-activity-journal
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the gnome-activity-journal package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-activity-journal\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2011-01-26 18:05+0100\n"
"PO-Revision-Date: 2010-10-31 00:57+0000\n"
"Last-Translator: Xuacu Saturio <xuacusk8@gmail.com>\n"
"Language-Team: Asturian <ast@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2011-01-27 04:52+0000\n"
"X-Generator: Launchpad (build 12177)\n"

#: ../gnome-activity-journal:85
msgid "print additional debugging information"
msgstr "amosar información de depuración adicional"

#: ../gnome-activity-journal:92
msgid "ERROR"
msgstr "ERROR"

#: ../gnome-activity-journal:92
msgid "Unable to connect to Zeitgeist:"
msgstr "Nun se pudo coneutar con Zeitgeist:"

#: ../src/supporting_widgets.py:83 ../src/main.py:304
msgid "Today"
msgstr "Güei"

#: ../src/supporting_widgets.py:86 ../src/main.py:307
msgid "Yesterday"
msgstr "Ayeri"

#: ../src/supporting_widgets.py:186
msgid "Go to Today"
msgstr "Dir a güei"

#: ../src/supporting_widgets.py:188
msgid "Go to the previous day "
msgstr ""

#: ../src/supporting_widgets.py:188
msgid "Go to the next day"
msgstr ""

#: ../src/supporting_widgets.py:520
msgid "Type here to search..."
msgstr "Escribe equí pa guetar..."

#: ../src/supporting_widgets.py:706
msgid "<b>Playing...</b>"
msgstr ""

#: ../src/supporting_widgets.py:820 ../src/supporting_widgets.py:830
#: ../src/supporting_widgets.py:1421
msgid "Preferences"
msgstr "Preferencies"

#: ../src/supporting_widgets.py:829
msgid "About"
msgstr ""

#: ../src/supporting_widgets.py:912 ../src/activity_widgets.py:551
msgid "Remove Pin"
msgstr "Quitar pinchu"

#: ../src/supporting_widgets.py:913 ../src/supporting_widgets.py:1287
msgid "Add Pin"
msgstr "Pinchar"

#: ../src/supporting_widgets.py:914
msgid "Delete item from Journal"
msgstr "Desaniciar elementu del Diariu"

#: ../src/supporting_widgets.py:915
msgid "Delete all events with this URI"
msgstr "Desaniciar tolos eventos con esti URI"

#: ../src/supporting_widgets.py:916 ../src/supporting_widgets.py:1355
msgid "More Information"
msgstr "Más Información"

#: ../src/supporting_widgets.py:928
msgid "Send To..."
msgstr "Unviar a…"

#: ../src/supporting_widgets.py:1198
msgid "Used With"
msgstr "Usáu con"

#: ../src/supporting_widgets.py:1282
msgid "Launch this subject"
msgstr "Llanzar esti asuntu"

#: ../src/supporting_widgets.py:1284
msgid "Delete this subject"
msgstr "Desaniciar esti asuntu"

#: ../src/supporting_widgets.py:1431
msgid "Active Plugins:"
msgstr "Complementos activos:"

#: ../src/supporting_widgets.py:1439
msgid "Plugins"
msgstr "Complementos"

#: ../src/supporting_widgets.py:1444
msgid "Show icon in system tray"
msgstr ""

#: ../src/supporting_widgets.py:1451
msgid "Configuration"
msgstr ""

#: ../src/main.py:143
msgid "<span size=\"larger\"><b>Loading Journal...</b></span>"
msgstr ""

#: ../src/main.py:318
#, python-format
msgid "%s to %s"
msgstr "%s a %s"

#: ../src/main.py:318 ../extra/gnome-activity-journal.desktop.in.h:1
msgid "Activity Journal"
msgstr "Diariu d'actividá"

#: ../src/content_objects.py:567 ../src/content_objects.py:569
msgid "{source._desc_sing} with {event.subjects[0].text}"
msgstr "{source._desc_sing} con {event.subjects[0].text}"

#: ../src/content_objects.py:568
msgid ""
"{source._desc_sing} with {event.subjects[0].text}\n"
"{event.subjects[0].uri}"
msgstr ""
"{source._desc_sing} con {event.subjects[0].text}\n"
"{event.subjects[0].uri}"

#: ../src/content_objects.py:579
msgid "Available"
msgstr "Disponible"

#: ../src/content_objects.py:580
msgid "Offline"
msgstr "Ensin conexón"

#: ../src/content_objects.py:581
msgid "Away"
msgstr "Ausente"

#: ../src/content_objects.py:582
msgid "Busy"
msgstr "Ocupáu"

#: ../src/content_objects.py:614 ../src/content_objects.py:619
#: ../src/content_objects.py:624
msgid "with"
msgstr "con"

#: ../src/content_objects.py:663
msgid "Time Tracker"
msgstr "Xestor de tiempu"

#: ../src/content_objects.py:737 ../src/content_objects.py:739
msgid "{source._desc_sing} from {event.subjects[0].text}"
msgstr "{source._desc_sing} de {event.subjects[0].text}"

#: ../src/content_objects.py:738
msgid ""
"{source._desc_sing} from {event.subjects[0].text}\n"
"{event.subjects[0].uri}"
msgstr ""
"{source._desc_sing} de {event.subjects[0].text}\n"
"{event.subjects[0].uri}"

#: ../src/content_objects.py:742
#, python-format
msgid " (%s Attachments)"
msgstr " (%s Axuntos)"

#: ../src/content_objects.py:766
msgid "{event.subjects[0].text}"
msgstr "{event.subjects[0].text}"

#: ../src/content_objects.py:767 ../src/content_objects.py:768
msgid ""
"Note\n"
"{event.subjects[0].text}"
msgstr ""
"Nota\n"
"{event.subjects[0].text}"

#: ../src/content_objects.py:786
msgid "{source._desc_sing} {event.subjects[0].text}"
msgstr ""

#: ../src/content_objects.py:787 ../src/content_objects.py:788
msgid ""
"GTG\n"
"{source._desc_sing} {event.subjects[0].text}"
msgstr ""

#: ../src/histogram.py:438
msgid "item"
msgid_plural "items"
msgstr[0] "elementu"
msgstr[1] "elementos"

#. TODO: Move this into Zeitgeist's library, implemented properly
#: ../src/config.py:238
msgid "Worked with a Video"
msgstr "Trabayó con un videu"

#: ../src/config.py:238
msgid "Worked with Videos"
msgstr "Trabayó con videos"

#: ../src/config.py:239
msgid "Worked with Audio"
msgstr "Trabayó con soníu"

#: ../src/config.py:240
msgid "Worked with an Image"
msgstr "Trabayó con una imaxen"

#: ../src/config.py:240
msgid "Worked with Images"
msgstr "Trabayó con imáxenes"

#: ../src/config.py:241
msgid "Edited or Read Document"
msgstr "Documentu editáu o lleíu"

#: ../src/config.py:241
msgid "Edited or Read Documents"
msgstr "Documentos editaos o lleíos"

#: ../src/config.py:242
msgid "Edited or Read Code"
msgstr "Códigu editáu o lleíu"

#: ../src/config.py:243
msgid "Conversation"
msgstr "Conversación"

#: ../src/config.py:243
msgid "Conversations"
msgstr "Conversaciones"

#: ../src/config.py:244
msgid "Visited Website"
msgstr "Sitiu web visitáu"

#: ../src/config.py:244
msgid "Visited Websites"
msgstr "Sitios web visitaos"

#: ../src/config.py:245 ../src/common.py:151
msgid "Email"
msgstr "Corréu electrónicu"

#: ../src/config.py:245
msgid "Emails"
msgstr "Correos electrónicos"

#: ../src/config.py:246
msgid "Todo"
msgstr ""

#: ../src/config.py:246
msgid "Todos"
msgstr ""

#: ../src/config.py:247
msgid "Other Activity"
msgstr "Otra actividá"

#: ../src/config.py:247
msgid "Other Activities"
msgstr "Otres actividaes"

#: ../src/config.py:248
msgid "Edited or Read Note"
msgstr "Nota editada o lleída"

#: ../src/config.py:248
msgid "Edited or Read Notes"
msgstr "Notes editaes o lleíes"

#: ../src/config.py:249
msgid "Software Development"
msgstr ""

#: ../src/config.py:249
msgid "Software Developments"
msgstr ""

#: ../src/Indicator.py:87
msgid "Hide/Show GAJ"
msgstr ""

#: ../src/common.py:144
msgid "Video"
msgstr "Videu"

#: ../src/common.py:145
msgid "Music"
msgstr "Música"

#: ../src/common.py:146
msgid "Document"
msgstr "Documentu"

#: ../src/common.py:147
msgid "Image"
msgstr "Imaxe"

#: ../src/common.py:148
msgid "Source Code"
msgstr "Códigu fonte"

#: ../src/common.py:149
msgid "Unknown"
msgstr "Desconocíu"

#: ../src/common.py:150
msgid "IM Message"
msgstr "Mensaxe nel intre"

#. TODO: In a future, I'd be cool to make the day partitions configurable.
#. This includes redefining what Morning/Afternoon/etc. are, but also
#. changing the categories entirely (eg. "Work", "Lunch time", etc.).
#. For this get_day_parts and other methods should take a timestamp of
#. the relevant day so they can account for weekends not having "Work", etc.
#: ../src/common.py:936
msgid "Morning"
msgstr "Mañana"

#: ../src/common.py:936
msgid "Afternoon"
msgstr "Tardi"

#: ../src/common.py:936
msgid "Evening"
msgstr "Nuechi"

#: ../src/activity_widgets.py:116
msgid "Switch to MultiView"
msgstr "Cambear a vista múltiple"

#: ../src/activity_widgets.py:627
msgid "<b>Name: </b>"
msgstr ""

#: ../src/activity_widgets.py:631
#, python-format
msgid ""
"\n"
"<b>MIME Type:</b> %s (%s)"
msgstr ""

#: ../src/activity_widgets.py:689
msgid "Switch to ThumbView"
msgstr "Cambear a vista de miniatures"

#: ../src/activity_widgets.py:1050
msgid "Switch to TimelineView"
msgstr "Cambear a vista de llinia de tiempu"

#: ../src/activity_widgets.py:1395
msgid "Pinned Items"
msgstr "Artículos pinchaos"

#: ../extra/gnome-activity-journal.desktop.in.h:2
msgid ""
"Browse a chronological log of your activities and easily find files, "
"contacts, etc."
msgstr ""
"Revisa un rexistru cronolóxicu de les actividaes y alcuentra fácilmente "
"ficheros, contautos, etc."

#: ../extra/gnome-activity-journal.schemas.in.h:1
msgid "Accessibility"
msgstr "Accesibilidá"

#: ../extra/gnome-activity-journal.schemas.in.h:2
msgid "Allow thumbnails as icons"
msgstr "Permitir miniatures como iconos"

#: ../extra/gnome-activity-journal.schemas.in.h:3
msgid "Amount of visible days"
msgstr "Cantidá de díes visibles"

#: ../extra/gnome-activity-journal.schemas.in.h:4
msgid ""
"Customized height for the main window, in pixels (requires restart). Can't "
"be bigger than the desktop's size."
msgstr ""
"Altor personalizáu de la ventana principal, en pixels (requier reaniciar). "
"Nun pue ser mayor que'l tamañu del escritoriu."

#: ../extra/gnome-activity-journal.schemas.in.h:5
msgid ""
"Customized width for the main window, in pixels (requires restart). Can't be "
"bigger than the desktop's size."
msgstr ""
"Anchor personalizáu de la ventana principal, en pixels (requier reaniciar). "
"Nun pue ser mayor que'l tamañu del escritoriu."

#: ../extra/gnome-activity-journal.schemas.in.h:6
msgid "The amount of days to show at once."
msgstr "Cantidá de díes qu'amosar a la vez."

#: ../extra/gnome-activity-journal.schemas.in.h:7
msgid ""
"Whether special accessibility options should be enabled (requires restart)."
msgstr ""
"Indica si tienen d'activase les opciones especiales d'accesibilidá (requier "
"reaniciar)."

#: ../extra/gnome-activity-journal.schemas.in.h:8
msgid ""
"Whether to allow showing little thumbnails next to each activity (requires "
"restart). If False, only generic mime-type icons will be used."
msgstr ""
"Indica si se permite amosar miniatures xunto a cada actividá (requier "
"reaniciar). Si ye falso, sólo s'usarán los iconos xenéricos del tipu MIME."

#: ../extra/gnome-activity-journal.schemas.in.h:9
msgid "Window height"
msgstr "Altor de la ventana"

#: ../extra/gnome-activity-journal.schemas.in.h:10
msgid "Window width"
msgstr "Anchor de la ventana"

#~ msgid "Powered by Zeitgeist"
#~ msgstr "Col sofitu de Zeitgeist"

#~ msgid "Search"
#~ msgstr "Guetar"

#~ msgid "Tags"
#~ msgstr "Etiquetes"
